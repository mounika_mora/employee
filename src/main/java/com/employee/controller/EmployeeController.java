package com.employee.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.dto.EmpoyeeDTO;
import com.employee.dto.ResponseDto;
import com.employee.entity.Employee;
import com.employee.service.EmployeeServiceImpl;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
@AllArgsConstructor
@RestController
@RequestMapping("/employee")
public class EmployeeController {
	
	EmployeeServiceImpl employeeServiceImpl;

	@PostMapping("/register")
	public ResponseEntity<ResponseDto> register(@Valid @RequestBody EmpoyeeDTO employeeDTO) {
		return new ResponseEntity<ResponseDto>(employeeServiceImpl.register(employeeDTO),
				HttpStatus.CREATED);
	}
	@GetMapping("/employees")
	public ResponseEntity<List<Employee>> getEmployees()
	{
		return new ResponseEntity<>(employeeServiceImpl.getEmployees(), HttpStatus.CREATED);
	}
	@GetMapping("/{id}")
    public ResponseEntity<EmpoyeeDTO> getEmployeeById(@PathVariable Long id) {
        return employeeServiceImpl.getEmployeeById(id);
    }
}
