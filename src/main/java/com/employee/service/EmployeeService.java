package com.employee.service;

import org.springframework.stereotype.Service;

import com.employee.dto.EmpoyeeDTO;
import com.employee.dto.ResponseDto;

@Service
public interface EmployeeService {
	ResponseDto register(EmpoyeeDTO employeeDTO);
}
