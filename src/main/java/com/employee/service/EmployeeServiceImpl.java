package com.employee.service;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.employee.dto.EmpoyeeDTO;
import com.employee.dto.ResponseDto;
import com.employee.entity.Employee;
import com.employee.exception.UserAlreadyExists;
import com.employee.repository.EmployeeRepository;


import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class EmployeeServiceImpl implements EmployeeService {
	EmployeeRepository employeeRepository;

	@Override
	public ResponseDto register(EmpoyeeDTO employeeDTO) {
		Optional<Employee> user = employeeRepository.findByposition(employeeDTO.getPosition());
		if (user.isPresent()) {
			throw new UserAlreadyExists("Employee already Registered");
		}
		Employee employee = new Employee();
		employee.setName(employeeDTO.getName());
		employee.setPosition(employeeDTO.getPosition());
		employeeRepository.save(employee);
		return ResponseDto.builder().httpStatus(201).message("Employee Registered sucessfully").build();

	}
	public List<Employee> getEmployees()
	{
		return employeeRepository.findAll();
		
	}
	public ResponseEntity<EmpoyeeDTO> getEmployeeById(Long id) {
        Optional<Employee> employeeOptional = employeeRepository.findById(id);
        if (employeeOptional.isPresent()) {
            Employee employee = employeeOptional.get();
            EmpoyeeDTO employeeDTO = new EmpoyeeDTO( employee.getName(), employee.getPosition());
            return ResponseEntity.ok(employeeDTO);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
